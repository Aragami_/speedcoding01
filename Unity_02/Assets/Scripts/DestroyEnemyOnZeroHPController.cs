﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-75)]
public class DestroyEnemyOnZeroHPController : MonoBehaviour
{
    public static DestroyEnemyOnZeroHPController Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
    }

    private Enemy[] m_enemies;
    private int currentEnemy = 0;

    private void Start()
    {
        m_enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy enemy in m_enemies)
        {
            enemy.enabled = false;
        }
        m_enemies[currentEnemy].enabled = true;
    }

    private void Update()
    {
        float currentHp = m_enemies[currentEnemy].CurrentHP;
        if (currentHp.Equals(0))
        {
            DestroyImmediate(m_enemies[currentEnemy]);
            currentEnemy++;
            m_enemies[currentEnemy].enabled = true;
        }
        m_enemies[currentEnemy].gameObject.name = "" + currentHp;
    }
}
