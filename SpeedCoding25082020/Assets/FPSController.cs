﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerMovementState
{
    ONGROUND = 0,
    INAIR
}

public class FPSController : MonoBehaviour
{
    private Rigidbody m_rig;
    private CapsuleCollider m_sp;
    public float speed;
    private PlayerMovementState m_state = PlayerMovementState.ONGROUND;
    private bool m_isRunning = false;
    public float runSpeedMultiplier;
    //collider

    private void Start()
    {
        m_rig = GetComponent<Rigidbody>();
        m_sp = GetComponent<CapsuleCollider>();
    }

    private void Update()
    {
        m_isRunning = m_state == PlayerMovementState.ONGROUND && Input.GetKey(KeyCode.LeftShift);
        if (Input.GetKeyDown(KeyCode.Space) && m_state == PlayerMovementState.ONGROUND)
        {
            m_rig.AddForce(Vector3.up * 200);
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            m_sp.center = new Vector3(m_sp.center.x, m_sp.center.y * 0.5f, m_sp.center.z);
            m_sp.height = m_sp.height * 0.5f;
        }

        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            m_sp.center = new Vector3(m_sp.center.x, m_sp.center.y * 2, m_sp.center.z);
            m_sp.height = m_sp.height * 2;

        }
    }

    private void FixedUpdate()
    {
        m_rig.velocity = new Vector3(Input.GetAxisRaw("Horizontal") * speed * (m_isRunning ? runSpeedMultiplier : 1), m_rig.velocity.y, Input.GetAxisRaw("Vertical") * speed * (m_isRunning ? runSpeedMultiplier : 1));
    }

    private void OnCollisionEnter(Collision _other)
    {
        if (_other.collider.CompareTag("Ground"))
        {
            m_state = PlayerMovementState.ONGROUND;
        }
    }

    private void OnCollisionExit(Collision _other)
    {
        if (_other.collider.CompareTag("Ground"))
        {
            m_state = PlayerMovementState.INAIR;
        }
    }
}
